﻿using Izio.Umbraco.Emailer.Interfaces;
using Izio.Umbraco.Emailer.Repositories;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace Izio.Umbraco.Emailer.Installation
{
    public class ServiceComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<IFormRepository, FormRepository>();
        }
    }
}
