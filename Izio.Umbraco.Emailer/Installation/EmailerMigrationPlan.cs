﻿using Umbraco.Core.Migrations;

namespace Izio.Umbraco.Emailer.Installation
{
    public class EmailerMigrationPlan : MigrationPlan
    {
        public EmailerMigrationPlan(): base("CustomDatabaseTable")
        {
            From(string.Empty).To<EmailerMigration>("1.0.0");
        }
    }
}
