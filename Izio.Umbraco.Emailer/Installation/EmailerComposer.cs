﻿using Umbraco.Core;
using Umbraco.Core.Composing;

namespace Izio.Umbraco.Emailer.Installation
{
    public class EmailerComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components().Append<EmailerComponent>();
        }
    }
}