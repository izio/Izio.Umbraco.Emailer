﻿using Izio.Umbraco.Emailer.Models;
using Umbraco.Core.Migrations;

namespace Izio.Umbraco.Emailer.Installation
{

    public class EmailerMigration : MigrationBase
    {
        public EmailerMigration(IMigrationContext context): base(context)
        {

        }

        public override void Migrate()
        {
            if (!TableExists("izioEmailerForms"))
            {
                Create.Table<Form>().Do();
            }
        }
    }
}
