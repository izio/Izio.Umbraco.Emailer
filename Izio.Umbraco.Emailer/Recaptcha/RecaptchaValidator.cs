﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Izio.Umbraco.Emailer.Recaptcha
{
    public class RecaptchaValidator
    {
        /// <summary>
        /// Validates a recaptcha response
        /// </summary>
        /// <param name="captchaResponse">the recaptcha response</param>
        /// <param name="secret">the client secret</param>
        /// <returns>RecaptchaValidationResult</returns>
        public static RecaptchaValidationResult IsValid(string captchaResponse, string secret)
        {
            if (string.IsNullOrWhiteSpace(captchaResponse))
            {
                return new RecaptchaValidationResult { Success = false };
            }

            var client = new HttpClient {BaseAddress = new Uri("https://www.google.com")};
            var values = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("secret", secret),
                new KeyValuePair<string, string>("response", captchaResponse)
            };
            var content = new FormUrlEncodedContent(values);
            var response = client.PostAsync("/recaptcha/api/siteverify", content).Result;
            var verificationResponse = response.Content.ReadAsStringAsync().Result;
            var verificationResult = JsonConvert.DeserializeObject<RecaptchaValidationResult>(verificationResponse);

            return verificationResult;
        }
    }
}
