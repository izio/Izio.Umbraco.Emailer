﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Izio.Umbraco.Emailer.Recaptcha
{
    public class RecaptchaValidationResult
    {
        /// <summary>
        /// Indicates whether the recaptcha validation was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// The timestamp of the validation request
        /// </summary>
        [JsonProperty("challenge_ts")]
        public string TimeStamp { get; set; }

        /// <summary>
        /// A list of error codes returned from the validation request
        /// </summary>
        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}
