﻿using System;
using System.Collections.Generic;
using Izio.Umbraco.Emailer.Interfaces;
using Izio.Umbraco.Emailer.Models;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Scoping;

namespace Izio.Umbraco.Emailer.Repositories
{
    public class FormRepository : IFormRepository
    {
        private readonly IScopeProvider _scopeProvider;

        public FormRepository(IScopeProvider scopeProvider)
        {
            _scopeProvider = scopeProvider;
        }

        #region IFormRepository

        public IEnumerable<Form> GetAll()
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var forms = database.Fetch<Form>("SELECT * FROM izioEmailerForms");

                scope.Complete();

                return forms;
            }
        }

        public Form GetById(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var form = database.SingleOrDefault<Form>("SELECT * FROM izioEmailerForms WHERE Id = @0", id);

                scope.Complete();

                return form;
            }
        }

        public Form GetByReference(Guid reference)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;
                var form = database.SingleOrDefault<Form>("SELECT * FROM izioEmailerForms WHERE Reference = @0", reference);

                scope.Complete();

                return form;
            }
        }

        public Form Save(Form form)
        {
            if (form.IsNew || form.IsDirty)
            {
                using (var scope = _scopeProvider.CreateScope())
                {
                    var database = scope.Database;

                    database.Save(form);

                    scope.Complete();
                }
            }

            return form;
        }

        public void Delete(int id)
        {
            using (var scope = _scopeProvider.CreateScope())
            {
                var database = scope.Database;

                database.Execute("DELETE FROM izioEmailerForms WHERE Id = @0", id);

                scope.Complete();
            }
        }

        #endregion
    }
}