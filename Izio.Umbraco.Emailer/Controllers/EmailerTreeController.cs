﻿using Izio.Umbraco.Emailer.Interfaces;
using System.Linq;
using System.Net.Http.Formatting;
using Umbraco.Core;
using Umbraco.Web.Actions;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace Izio.Umbraco.Emailer.Controllers
{
    [Tree(Constants.Applications.Settings, "Emailer", TreeGroup = Constants.Trees.Groups.Settings, TreeTitle ="Emailer Forms", SortOrder = 100)]
    [PluginController("Emailer")]
    public class EmailerTreeController : TreeController
    {
        private readonly IFormRepository _repository;

        public EmailerTreeController(IFormRepository formRepository)
        {
            _repository = formRepository;
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();

            nodes.AddRange(_repository.GetAll().Select(contactForm => CreateTreeNode(contactForm.Id.ToString(), "-1", queryStrings, contactForm.Name)));

            return nodes;
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();

            if (id == Constants.System.Root.ToInvariantString())
            {
                menu.Items.Add<ActionNew>(Services.TextService);
                menu.Items.Add(new RefreshNode(Services.TextService, true));

                return menu;
            }

            menu.Items.Add<ActionDelete>(Services.TextService, opensDialog: true);

            return menu;
        }

        protected override TreeNode CreateRootNode(FormDataCollection queryStrings)
        {
            var root = base.CreateRootNode(queryStrings);

            root.RoutePath = string.Format("{0}/{1}/{2}", Constants.Applications.Settings, "Emailer", "default");
            root.Icon = "icon-folder";
            root.HasChildren = true;

            return root;
        }
    }
}
