﻿using Izio.Umbraco.Emailer.Interfaces;
using Izio.Umbraco.Emailer.Recaptcha;
using Izio.Umbraco.Emailer.ViewModels;
using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.Helpers;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Mvc;

namespace Izio.Umbraco.Emailer.Controllers
{
    [PluginController("Emailer")]
    public class EmailerController : SurfaceController
    {
        private readonly IFormRepository _repository;

        public EmailerController(IFormRepository formRepository)
        {
            _repository = formRepository;
        }

        [ChildActionOnly]
        public ActionResult Render(string formReference)
        {
            var form = _repository.GetByReference(new Guid(formReference));

            if (form != null)
            {
                return PartialView("Form", new EmailerViewModel { SubmissionStamp = Security.GenerateSecurityToken(), FormReference = form.Reference.ToString() });
            }

            return null;
        }

        [HttpPost]
        [NotChildAction]
        public ActionResult Submit(EmailerViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AntiForgery.Validate();

                    var form = _repository.GetByReference(new Guid(model.FormReference));

                    if (form != null)
                    {
                        var address = form.DestinationAddress;
                        var submissionLimit = form.SubmissionLimit;
                        var captchaResponse = Request.Form["g-Recaptcha-Response"];

                        if (Security.ValidateSecurityToken(model.SubmissionStamp, submissionLimit))
                        {
                            var result = string.IsNullOrEmpty(ConfigurationManager.AppSettings["RecaptchaSiteKey"]) ? new RecaptchaValidationResult { Success = true } : RecaptchaValidator.IsValid(captchaResponse, ConfigurationManager.AppSettings["RecaptchaSecret"]);

                            if (result.Success)
                            {
                                try
                                {
                                    var client = new SmtpClient();
                                    var message = new MailMessage(model.Email, address)
                                    {
                                        Subject = FormatTemplate(form.TemplateSubject, model),
                                        Body = FormatTemplate(form.TemplateBody, model),
                                        IsBodyHtml = true
                                    };

                                    client.Send(message);

                                    if (form.ResponderEnabled)
                                    {
                                        var response = new MailMessage(address, model.Email)
                                        {
                                            Subject = FormatTemplate(form.ResponderSubject, model),
                                            Body = FormatTemplate(form.ResponderBody, model),
                                            IsBodyHtml = true
                                        };

                                        client.Send(response);
                                    }

                                    TempData[model.FormReference + "EmailSent"] = true;
                                    TempData[model.FormReference + "ConfirmationMessage"] = form.ConfirmationMessage;

                                    return RedirectToCurrentUmbracoPage();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error<EmailerController>("Exception thrown sending email:"+ ex.Message, ex);

                                    ModelState.AddModelError("", "There was an error sending your email, please try again later.");
                                }
                            }
                            else
                            {
                                Logger.Error<EmailerController>("Recaptcha validation failed sending email");

                                ModelState.AddModelError("", "Recaptcha validation failed");
                            }
                        }
                        else
                        {
                            Logger.Error<EmailerController>("Submission throttle breached");

                            ModelState.AddModelError("", $"Submission throttle breached, you must wait {form.SubmissionLimit} seconds before submitting the form.");
                        }
                    }
                    else
                    {
                        Logger.Error<EmailerController>("Form not specified, cannot send email");

                        ModelState.AddModelError("", "There was an error sending your email, please refresh the page and try again.");
                    }
                }
                catch (HttpAntiForgeryException ex)
                {
                    Logger.Error<EmailerController>("Anti forgery validation failed sending email", ex);

                    ModelState.AddModelError("", "There was an error sending your email, please refresh the page and try again.");
                }
                catch (Exception ex)
                {
                    Logger.Error<EmailerController>("An error occurred sending email: "+ ex.Message, ex);

                    ModelState.AddModelError("", "There was an error sending your email, please refresh the page and try again.");
                }
            }

            return CurrentUmbracoPage();
        }

        static string FormatTemplate(string template, EmailerViewModel model)
        {
            return template.Replace("[name]", model.Name).Replace("[email]", model.Email).Replace("[subject]", model.Subject).Replace("[body]", model.Body);
        }
    }
}