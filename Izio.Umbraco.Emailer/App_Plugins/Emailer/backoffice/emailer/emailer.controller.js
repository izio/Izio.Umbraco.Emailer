﻿angular.module("umbraco")
    .controller("Emailer.CreateController",
        function ($scope, $http, $location, notificationsService, navigationService) {

            $scope.loaded = true;
            $scope.form = {};

            $scope.cancel = function () {
                $scope.create.$dirty = false;
                navigationService.hideMenu();
            };

            $scope.save = function (form) {

                form.SubmissionLimit = 10;
                form.ConfirmationMessage = "Thank you for your email.";
                form.TemplateSubject = "[subject]";
                form.TemplateBody = "[body]";

                $http.post("/umbraco/backoffice/api/EmailerApi/Save", form).then(function successCallback(response) {
                    navigationService.hideMenu();
                    navigationService.syncTree({ tree: "Emailer", path: -1 + ',' + response.data.Id, forceReload: true, activate: true });
                    notificationsService.success("Success, the form " + form.Name + " has been saved");

                    $scope.create.$dirty = false;
                    
                    $location.path("/settings/Emailer/edit/" + response.data.Id);
                }, function errorCallback(response) {
                    notificationsService.error("Error", "Failed to create new emailer form, please try again later / refresh page");
                });
            };
        })
    .controller("Emailer.EditController",
        function ($scope, $routeParams, $http, notificationsService, navigationService) {
            $scope.ConfirmationMessage = {
                label: "bodyText",
                view: "/umbraco/views/propertyeditors/rte/rte.html",
                config: {
                    editor: {
                        toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                        stylesheets: [],
                        dimensions: { height: 400 }
                    }
                },
                value: "<p>some text</p>"
            };
            $scope.TemplateBody = {
                label: "bodyText",
                view: "/umbraco/views/propertyeditors/rte/rte.html",
                config: {
                    editor: {
                        toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                        stylesheets: [],
                        dimensions: { height: 400 }
                    }
                }
            };
            $scope.ResponderBody = {
                label: "bodyText",
                view: "/umbraco/views/propertyeditors/rte/rte.html",
                config: {
                    editor: {
                        toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                        stylesheets: [],
                        dimensions: { height: 400 }
                    }
                }
            };
            $scope.loaded = false;

            $http.get("/umbraco/backoffice/api/EmailerApi/GetById/" + $routeParams.id).then(function successCallback(response) {
                $scope.form = {
                    Name: response.data.Name,
                    Reference: response.data.Reference,
                    DestinationAddress: response.data.DestinationAddress,
                    SubmissionLimit: response.data.SubmissionLimit.toString(),
                    TemplateSubject: response.data.TemplateSubject,
                    ResponderEnabled: response.data.ResponderEnabled,
                    ResponderAddress: response.data.ResponderAddress,
                    ResponderSubject: response.data.ResponderSubject
                };

                $scope.ConfirmationMessage.value = response.data.ConfirmationMessage;
                $scope.TemplateBody.value = response.data.TemplateBody;
                $scope.ResponderBody.value = response.data.ResponderBody;

                $scope.loaded = true;
            }, function errorCallback(response) {
                notificationsService.error("Error", "Failed to load emailer form, please try again later / refresh page");
            });

            $scope.save = function (form) {

                form.Id = $routeParams.id;
                form.ConfirmationMessage = $scope.ConfirmationMessage.value;
                form.ResponderBody = $scope.ResponderBody.value;
                form.TemplateBody = $scope.TemplateBody.value;
                form.IsDirty = true;

                $http.post("/umbraco/backoffice/api/EmailerApi/Save", form).then(function successCallback(response) {
                    navigationService.hideMenu();
                    navigationService.syncTree({ tree: "Emailer", path: [-1, -1], forceReload: true });
                    notificationsService.success("Success, the form " + form.Name + " has been saved");

                    $scope.edit.$dirty = false;
                }, function errorCallback(response) {
                    notificationsService.error("Error", "Failed to save emailer form, please try again later / refresh page");
                });
            };
        })
    .controller("Emailer.DeleteController",
        function ($scope, $routeParams, $http, $location, notificationsService, navigationService) {

            $scope.loaded = true;

            $scope.cancel = function () {
                navigationService.hideMenu();
            };

            $scope.delete = function (id) {
                $http.post("/umbraco/backoffice/api/EmailerApi/Delete/" + id).then(function successCallback(response) {
                        navigationService.hideMenu();
                        navigationService.syncTree({ tree: "Emailer", path: [-1, -1], forceReload: true });
                        notificationsService.success("Success, the form has been deleted");

                        $location.path("/settings/Emailer/default");
                    },
                    function errorCallback(response) {
                        notificationsService.error("Error",
                            "Failed to delete emailer form, please try again later / refresh page");
                    });
            };
        });